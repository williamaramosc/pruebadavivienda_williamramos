import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Vivi001Component } from './screen/vivi001/vivi001.component';
import { HeaderComponent } from './components/header/header.component';
import { CardComponent } from './components/card/card.component';
import { RectangleComponent } from './components/rectangle/rectangle.component';

@NgModule({
  declarations: [
    AppComponent,
    Vivi001Component,
    HeaderComponent,
    CardComponent,
    RectangleComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
