import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Vivi001Component } from './screen/vivi001/vivi001.component';

const routes: Routes = [

  {
    path: '',component:Vivi001Component
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { 


}
