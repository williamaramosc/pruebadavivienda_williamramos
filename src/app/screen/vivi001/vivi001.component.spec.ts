import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Vivi001Component } from './vivi001.component';

describe('Vivi001Component', () => {
  let component: Vivi001Component;
  let fixture: ComponentFixture<Vivi001Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Vivi001Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Vivi001Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
