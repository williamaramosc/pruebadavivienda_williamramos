import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-vivi001',
  templateUrl: './vivi001.component.html',
  styleUrls: ['./vivi001.component.sass']
})
export class Vivi001Component implements OnInit {

  mostrar:boolean = false;

  options: any[] = [
    { popUp: false, isOption: false, title: "Transferencias y avances", name: "WILLIAM ALEJANDRO RAMOS CAROSI" },
    { popUp: false, isOption: true, name: "Entre mis productos Davivienda" },
    { popUp: false, isOption: true, name: "A otras cuentas Davivienda" },
    { popUp: false, isOption: true, name: "A DaviPlata" },
    { popUp: false, isOption: true, name: "A cuentas otros bancos" },
    { popUp: false, isOption: true, name: "Desde y hacía mis bolsillos" },
    { popUp: false, isOption: true, name: "Giros internacionales" },
    { popUp: false, isOption: true, name: "Avances" },
    { popUp: true, isOption: true, name: "Transferencia a DaviPlata por comando de voz" }
  ];

  constructor() { }

  ngOnInit(): void {
  }

  action(data: any) {
    console.log('dsada');
    if (data.popUp) {
      // document.getElementById("popUp")!.style.transform = "translateY(0)";
      this.mostrar = true;
      console.log(this.mostrar = true);
      document.getElementById("popUp")!.style.opacity = "1";
      document.getElementById("popUp")!.style.height = "100%";
    }
  }

}
