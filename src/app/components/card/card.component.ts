import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.sass']
})

export class CardComponent implements OnInit {
  
  @Input() data: any;
  display:boolean = false;
  @Output() action = new EventEmitter();

  ngOnInit(): void {
  }

  onPress() {
    this.action.emit(this.data)
  }

}
