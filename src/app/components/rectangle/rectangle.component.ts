import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-rectangle',
  templateUrl: './rectangle.component.html',
  styleUrls: ['./rectangle.component.sass']
})
export class RectangleComponent implements OnInit {

  @Input() mostrarPopUp:any
  
  // States
  display: boolean = true;
  step1: boolean = true;
  step2: boolean = false;
  step3: boolean = false;
  step4: boolean = false;
  step5: boolean = false;
  circleButtom: boolean = true;

  // Strings Labels
  name: String = "Hola William,"
  textInfo: String = "Tenga en cuenta que para realizar una transferencia por voz a DaviPlata, sus Cuentas deben estar activas y con saldo."
  textInfo2: String = "Por lo general usted realiza transferencias a los siguientes destinos DaviPlata:"
  textInfo3: String = "Si desea transferir a algunos de estos destino diga el destino o nueva transferencia:"
  voiceOption: String = "“Transferir a mamá”";
  transferResult: String = "“Quiero hacer la transferencia desde mi Cuenta <strong>de Ahorros terminada en 1234”</strong>";

  // Arrays
  options: any[] = [
    { name: "Mamá", number: "320 123 4567" },
    { name: "Arriendo", number: "321 555 1234" },
    { name: "Oficina", number: "321 555 1234" }
  ];

  accounts: any[] = [
    {
      accountType: "Cuenta de Ahorros", numberAccount: "1234 5678 7865 1234", balance: "$999.999.999,00", availableBalance: "Saldo disponible"
    },
    {
      accountType: "Cuenta Corriente", numberAccount: "3456 7890 2233 4567", balance: "$999.999.999,00", availableBalance: "Saldo disponible"
    },
  ];

  textInfoStep4: String[] = [
    "Indique alias o los cuatro últimos dígitos de la Cuenta desde la cual realizará la transferencia:",
    "Cuenta terminada en 1234"
  ];

  transferDetails: String[] = [
    "Para continuar su transacción diga <strong>Transferir</strong>, de lo contrario indique el campo que desea modificar:",
    "Modificar destino"
  ];

  transferOptions: String[] = [
    "Transferir a mamá",
    "Transferir a otro Daviplata"
  ];

  destinyLabels: String[] = [
    "Origen",
    "Destino",
    "Cuenta de Ahorros",
    "Mamá",
    "6216 9618 9213 1234",
    "320 222 9988"
  ];

  transactionValue: String[] = [
    "Valor",
    "$100.000,00",
    "Costo de la transacción:",
    "$ 0"
  ];

  transactionConfirmation: String[] = [
    "William, ha realizado la transferencia exitosa a DaviPlata.</br></br>",
    "¿Desea realizar otra transferencia a DaviPlata?"
  ];

  transactionVoucher: String[] = [
    "Número de comprobante",
    "Día: 10/12/2020",
    "304003",
    "Hora: 4:00 p.m."
  ];
  constructor() { }

  ngOnInit(): void {
  }

  // Method to control the visibility of elements regarding the rectangle component
  micButton() {
    if (this.step1) {
      this.display = false;
      document.getElementById("popUpRectangle")!.style.height = "74%";
      document.getElementById("popUpRectangle")!.style.transition = "1s";
      this.step1 = false;
      this.step2 = true;

    } else if (this.step2) {
      this.circleButtom = false;
      document.getElementById("popUpRectangle")!.style.height = "82%";

      // Simulation when the user use the command voice functionality
      setTimeout(() => {
        document.getElementById("popUpRectangle")!.style.height = "60%";
        this.step2 = false;
        this.step3 = true;

        setTimeout(() => {
          this.circleButtom = true;
          document.getElementById("popUpRectangle")!.style.height = "59%";
        }, 2000);
      }, 2000);

    } else if (this.step3) {
      this.step3 = false;
      this.step4 = true;
      document.getElementById("popUpRectangle")!.style.height = "68%";
    } else {
      this.step5 = true;
      this.step4 = false;
      document.getElementById("popUpRectangle")!.style.height = "75%";
    }

  }

}
